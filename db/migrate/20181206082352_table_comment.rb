class TableComment < ActiveRecord::Migration[5.2]
  def change
    create_table :comments
    add_column :comments, :content, :string
  end
end
